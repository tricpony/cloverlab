//
//  RestaurantSummaryViewController.swift
//  CloverLab
//
//  Created by aarthur on 1/27/21.
//

import UIKit

/// Landing view controller.
class RestaurantSummaryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pinWheel: UIActivityIndicatorView!

    var dataSource: [Model]? {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.bringSubviewToFront(pinWheel)
        tableView.tableFooterView = UIView()
        title = "Restaurants"
        performService()
    }

    // MARK: - Network

    /// Fetch Restaurants.
    func performService() {
        pinWheel.isHidden = false
        pinWheel.startAnimating()
        _ = ServiceManager.sharedService.startServiceAt(urlString: API.serviceAddress) { [weak self] result in
            DispatchQueue.main.async {
                self?.pinWheel.stopAnimating()
            }

            switch result {
            case .success(let payload):
                let fetchedObjects = JsonUtility<[Restaurant]>.parseJSON(payload)
                DispatchQueue.main.async {
                    self?.dataSource = fetchedObjects?.sorted()
                }
            case .failure(let error):
                self?.handleError(error)
            }
        }
    }

    /// Display error alert panel..
    /// - Parameters:
    ///   - error: The error to display the contents of.
    func handleError(_ error: ServiceError) {
        DispatchQueue.main.async { [weak self] in
            self?.presentAlert(title: "Alert", message: error.errorDescription, buttonTitles: ["OK"], completion: nil)
        }
    }

    // MARK: - Table View

    /// Determine the cell resuse identifier.
    /// - Parameters:
    ///   - indexPath: Index path of next cell.
    /// - Returns: Proper cell resuse identifier.
    func cellIdentifier(at indexPath: IndexPath) -> String {
        return RestaurantSummaryTableCell.reuseIdentifier
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }

    /// Create the next cell.
    /// - Parameters:
    ///   - tableView: Destination table view.
    ///   - indexPath: Index path of next cell.
    /// - Returns: Proper cell.
    func nextCellForTableView(_ tableView: UITableView, at indexPath: IndexPath) -> SummaryCell? {
        return tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(at: indexPath)) as? SummaryCell
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = nextCellForTableView(tableView, at: indexPath) else { return UITableViewCell() }
        let restaurant = dataSource?[indexPath.row]
        cell.fillCell(restaurant)
        return cell
    }
}
