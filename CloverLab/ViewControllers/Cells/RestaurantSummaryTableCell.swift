//
//  RestaurantSummaryTableCell.swift
//  CloverLab
//
//  Created by aarthur on 1/27/21.
//

import UIKit

class RestaurantSummaryTableCell: UITableViewCell, SummaryCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    static let reuseIdentifier = "RestaurantSummaryTableCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectedBackgroundView = UIView(frame: contentView.bounds)
        selectedBackgroundView?.backgroundColor = .clear
        nameLabel.highlightedTextColor = UIColor(named: "RestaurantTitleSelected")
        categoryLabel.highlightedTextColor = UIColor(named: "RestaurantCategorySelected")
        aboutLabel.highlightedTextColor = UIColor(named: "RestaurantAboutSelected")
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        selectedBackgroundView?.backgroundColor = UIColor(named: "CellBackgroundSelected")
        nameLabel.isHighlighted = highlighted
        categoryLabel.isHighlighted = highlighted
        aboutLabel.isHighlighted = highlighted
    }

    func fillCell(_ model: Model?) {
        nameLabel.text = model?.name
        categoryLabel.text = model?.displayCategory
        aboutLabel.text = model?.about
    }
}
