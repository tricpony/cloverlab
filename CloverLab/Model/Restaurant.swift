//
//  Restaurant.swift
//  CloverLab
//
//  Created by aarthur on 1/27/21.
//

import Foundation

/// Model type.
struct Restaurant: Model, Decodable, Comparable {
    var id: Int
    var name: String
    var about: String
    var category: String
    var displayCategory: String {
        category.capitalized
    }

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case about = "description"
        case category
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.about = try container.decode(String.self, forKey: .about)
        self.category = try container.decode(String.self, forKey: .category)
    }
    
    // MARK: - Comparable

    static func <(lhs: Restaurant, rhs: Restaurant) -> Bool {
        lhs.name < rhs.name
    }
}
