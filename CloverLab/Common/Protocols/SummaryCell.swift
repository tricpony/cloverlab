//
//  SummaryCell.swift
//  CloverLab
//
//  Created by aarthur on 1/27/21.
//

import UIKit

/// A protocol to unify all summary cells.
protocol SummaryCell where Self: UITableViewCell {
    
    /// Populate the cell.
    /// - Parameters:
    ///   - model: Model object used to fill the cell.
    func fillCell(_ model: Model?)
}
