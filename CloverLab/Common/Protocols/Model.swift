//
//  Model.swift
//  TeladocLab
//
//  Created by aarthur on 8/29/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation

/// This is a protocol for all model classes.
protocol Model {
    var id: Int { get set }
    var name: String { get set }
    var about: String { get set }
    var displayCategory: String { get }
}
