# README #

Comments about what you will find in CloverLab.

### Clone ###
https://tricpony@bitbucket.org/tricpony/cloverlab.git

### Language ###
The project is written in Swift.

### Groups ###
Application - Contains AppDelegate.
ViewControllers - Contains implementation for Restaurant cell and view controller.
Model - Contains Restaurant model class.
Common - Contains resources that could potentially be refactored into separate modules.

### Unit Tests ###
Some unit tests have been included.
