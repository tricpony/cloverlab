//
//  JsonUtilityTests.swift
//  CloverLab
//
//  Created by aarthur on 1/27/21.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import XCTest
@testable import CloverLab

class JsonUtilityTests: XCTestCase {
    let modelArrayJson = "[{\"id\":0, \"name\":\"arrayName\", \"category\":\"categoryValue\", \"description\": \"descriptionValue\"}]"
    let modelDictinoaryJson = "{\"id\":0, \"name\":\"arrayName\", \"category\":\"categoryValue\", \"description\": \"descriptionValue\"}"
    let modelBogus = "{\"bogus\"}"

    func testParsingJsonArray() {
        let payload = Data(modelArrayJson.utf8)
        let modelObjects = JsonUtility<[MockModel]>.parseJSON(payload)
        XCTAssertTrue(type(of: modelObjects) == [MockModel]?.self)
    }
    
    func testParsingJsonSingle() {
        let payload = Data(modelDictinoaryJson.utf8)
        let modelObjects = JsonUtility<MockModel>.parseJSON(payload)
        XCTAssertTrue(type(of: modelObjects) == MockModel?.self)
    }
    
    func testParsingJsonNil() {
        let payload = Data(modelBogus.utf8)
        let modelObjects = JsonUtility<Restaurant>.parseJSON(payload)
        XCTAssertNil(modelObjects)
    }
    
    func testParsingJsonNotNil() {
        let payload = Data(modelArrayJson.utf8)
        let modelObjects = JsonUtility<[Restaurant]>.parseJSON(payload)
        XCTAssertNotNil(modelObjects)
    }
}
