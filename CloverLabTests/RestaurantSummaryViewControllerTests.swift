//
//  RestaurantSummaryViewControllerTests.swift
//  CloverLabTests
//
//  Created by aarthur on 1/28/21.
//

import XCTest
@testable import CloverLab

class RestaurantSummaryViewControllerTests: XCTestCase {
    var testViewController: RestaurantSummaryViewController!

    override func setUpWithError() throws {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        testViewController = storyboard.instantiateViewController(withIdentifier: "MasterViewScene") as? RestaurantSummaryViewController
    }
    
    override func tearDownWithError() throws {
        testViewController = nil
    }

    func testTitle() {
        let title = "My Title"
        testViewController.title = title
        XCTAssertEqual(title, testViewController.title)
    }

    func testPinWheelExists() {
        _ = testViewController.view
        XCTAssertNotNil(testViewController.pinWheel)
    }
    
    func testPinWheelIsNotHidden() {
        _ = testViewController.view
        XCTAssertFalse(testViewController.pinWheel.isHidden)
    }
    
    func testTableViewRowsReferenced() {
        let expectation = XCTestExpectation(description: "Verify table data source.")
        let mockDelegate = MockMapViewDelegate(expectation: expectation)
        _ = testViewController.view
        testViewController.tableView.delegate = mockDelegate
        testViewController.tableView.dataSource = mockDelegate
        wait(for: [expectation], timeout: 1.0)
        XCTAssertNotNil(testViewController.dataSource)
    }
}

class MockMapViewDelegate: NSObject, UITableViewDataSource, UITableViewDelegate {
    var expectation: XCTestExpectation!
    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        expectation?.fulfill()
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}

