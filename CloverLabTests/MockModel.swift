//
//  MockModel.swift
//  CloverLab
//
//  Created by aarthur on 1/27/21.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation
@testable import CloverLab

struct MockModel: Model, Decodable {
    var id = 0
    var name = "name"
    var about = "about"
    var category = "category"
    var displayCategory: String {
        category.capitalized
    }

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case about = "description"
        case category
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.about = try container.decode(String.self, forKey: .about)
        self.category = try container.decode(String.self, forKey: .category)
    }
}
