//
//  APITests.swift
//  CloverLabTests
//
//  Created by aarthur on 1/28/21.
//

import XCTest
@testable import CloverLab

class APITests: XCTestCase {
    func testServiceAddressPass() {
        XCTAssertEqual(API.serviceAddress, "http://www.json-generator.com/api/json/get/bQwCjWxBDS")
    }
    
    func testServiceAddressFail() {
        XCTAssertNotEqual(API.serviceAddress, "bogus")
    }
}
