//
//  RestaurantTests.swift
//  CloverLabTests
//
//  Created by aarthur on 2/1/21.
//

import XCTest
@testable import CloverLab

class RestaurantTests: XCTestCase {
    let modelDictinoaryJson = "{\"id\":0, \"name\":\"arrayName\", \"category\":\"categoryValue\", \"description\": \"descriptionValue\"}"

    func testRestaurantIntegrity() {
        let payload = Data(modelDictinoaryJson.utf8)
        let modelObject = JsonUtility<Restaurant>.parseJSON(payload)
        XCTAssertEqual(modelObject?.id, 0)
        XCTAssertEqual(modelObject?.name, "arrayName")
        XCTAssertEqual(modelObject?.category, "categoryValue")
        XCTAssertEqual(modelObject?.about, "descriptionValue")
    }
}
